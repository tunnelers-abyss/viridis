local S = technic.getter

viridis = {}

technic.register_recipe_type("viridis_transformer", { 
	description = S("Viridis Transformation"),
    input_size = 1,
})

function technic.register_viridis_transformer_recipe(data)
	data.time = data.time or 10
	technic.register_recipe("viridis_transformer", data)
end
if technic.plus then
	technic.register_base_machine("viridis:hv_viridis_transformer", {
		typename = "viridis_transformer",
		description = S("@1 Viridis Transformer", S("HV")),
		tier = "HV",
		demand = { 3000, 2700, 2400 },
		speed = 1,
		upgrade = 1,
		tube = 1,
		tube_sides = { nil, nil, nil, nil, nil }
	})
elseif minetest.get_modpath("elements") then
	function elements.register_viridis_transformer(data)
		data.typename = "viridis_transformer"
		data.machine_name = "viridis_transformer"
		data.machine_desc = S("%s Viridis Transformer")
		technic.register_base_machine(data)
	end

	elements.register_viridis_transformer({ modname = "viridis", tier = "HV", demand = { 3000, 2700, 2400 }, speed = 1, tube = 1, upgrade = 1, tube_sides = { nil, nil, nil, nil, nil } })
else
	function technic.register_viridis_transformer(data)
		data.typename = "viridis_transformer"
		data.machine_name = "viridis_transformer"
		data.machine_desc = S("%s Viridis Transformer")
		technic.register_base_machine(data)
	end

	technic.register_viridis_transformer({
		tier = "HV",
		demand = { 3000, 2700, 2400 },
		speed = 1,
		upgrade = 1,
		tube = 1,
		tube_sides = { nil, nil, nil, nil, nil }
	})
end
minetest.register_craftitem("viridis:ingot", {
	description = "Viridis Ingot",
	inventory_image = "viridis_ingot.png",
})

minetest.register_craftitem("viridis:chunk", {
	description = "Viridis Chunk",
	inventory_image = "viridis_chunk.png",
        stack_max = 9009,
})

minetest.register_craftitem("viridis:grain", {
	description = "Viridis Grain",
	inventory_image = "viridis_grain.png",
        stack_max = 107910,
})

minetest.register_node("viridis:block", {
	description = "Viridis Block",
	tiles = {"viridis_block.png", "viridis_block.png^[transform3", "viridis_block.png^[transform1", "viridis_block.png", "viridis_block.png^[transform3", "viridis_block.png^[transform2"},
	groups = {cracky = 1},
	is_ground_content = false,
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_craft({
	recipe = {
		{"viridis:ingot", "viridis:ingot", "viridis:ingot"},
		{"viridis:ingot", "viridis:ingot", "viridis:ingot"},
		{"viridis:ingot", "viridis:ingot", "viridis:ingot"},
	},
	output = "viridis:block"
})

minetest.register_craft({
	recipe = {"viridis:block"},
	type = "shapeless",
	output = "viridis:ingot 9"
})

minetest.register_craft({
	output = 'viridis:hv_viridis_transformer',
	recipe = {
		{'technic:composite_plate', 'xtraores:unobtainium_block', 'technic:composite_plate'},
		{'underch:emerald_block', 'technic:hv_compactor', 'underch:saphire_block'},
		{'ethereal:crystal_ingot', 'technic:hv_cable', 'ethereal:crystal_ingot'},
	}
})

technic.register_compactor_recipe({input = {"viridis:ingot 9"}, output = "viridis:block"})
technic.register_compactor_recipe({input = {"viridis:chunk 91"}, output = "viridis:ingot"})
technic.register_compactor_recipe({input = {"viridis:grain 1090"}, output = "viridis:ingot"})

technic.register_viridis_transformer_recipe({input = {"moreblocks:cobble_condensed 999"}, output = "viridis:ingot"})
technic.register_viridis_transformer_recipe({input = {"moreblocks:cobble_compressed 99"}, output = "viridis:chunk"})
technic.register_viridis_transformer_recipe({input = {"condensed:desert_stone 999"}, output = "viridis:ingot"})
technic.register_viridis_transformer_recipe({input = {"compressed:desert_stone 99"}, output = "viridis:chunk"})
technic.register_viridis_transformer_recipe({input = {"moreblocks:dirt_compressed 99"}, output = "viridis:chunk"})

if minetest.get_modpath("elements") then
	technic.register_viridis_transformer_recipe({ input = { "elements:oxygen 5940" }, output = "viridis:grain" })
end

for id, s in pairs(underch.stone.defs) do
	technic.register_viridis_transformer_recipe({input = {"condensed:"..id.." 999"}, output = "viridis:ingot"})
	technic.register_viridis_transformer_recipe({input = {"compressed:"..id.." 99"}, output = "viridis:chunk"})
end

minetest.register_craft({
	output = 'digtron:digger 8',
	type = 'shapeless',
	recipe = {'digtron:digtron_core', 'viridis:ingot'},
})

